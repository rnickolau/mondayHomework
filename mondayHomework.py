# -*- coding: utf-8 -*-

class dataPoint:

    def __init__(self, time, eNodeBName, cellName, localCellID, abnormRelTot, abnormRelMME, normRel):
        self.time = time
        self.eNodeBName = eNodeBName
        self.cellName = cellName
        self.localCellID = localCellID
        self.abnormRelTot = abnormRelTot
        self.abnormRelMME = abnormRelMME
        self.normRel = normRel


import pyrebase
import zipfile
import openpyxl
import re, io

config = {
  "apiKey": "AIzaSyCUXQ-RDCRGzxTJ8DJwKc5VHhAMYAv6Kc4",
  "authDomain": "aoc-evo-178814.firebaseapp.com",
  "databaseURL": "https://aoc-evo-178814.firebaseio.com",
  "projectId": "aoc-evo-178814",
  "storageBucket": "aoc-evo-178814.appspot.com",
  "serviceAccount": "aoc-evo-178814-firebase-adminsdk-64yys-ecbb40a7a3.json"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
user = auth.sign_in_with_email_and_password("aocevo@aoc.com", "a0c3v0")
storage = firebase.storage()
dataset_url = storage.child("AoCBootcampDataset.zip").get_url(user['idToken'])
storage.child("AoCBootcampDataset.zip").download("AoCBootcampDataset.zip")

#with zipfile.ZipFile("AoCBootcampDataset.zip", 'r') as zfile:
#    zfile.extractall()
    

with zipfile.ZipFile("AoCBootcampDataset.zip", 'r') as zfile:
    for name in zfile.namelist():
        if re.search(r'\.zip$', name) != None:
            zfiledata = io.BytesIO(zfile.read(name))
            with zipfile.ZipFile(zfiledata) as zfile2:
                zfile2.extractall()


dataPointList = []

wb = openpyxl.load_workbook('DropCall_Band2_Query_Result_LBHON_Nov25_Dec1.xlsx')
ws = wb.active
for x in range(2, ws.max_row):
    time = ws.cell(row=x, column=1).value
    eNodeBName = ws.cell(row=x, column=2).value
    cellName = ws.cell(row=x, column=4).value
    localCellID = ws.cell(row=x, column=5).value
    abnormRelTot = ws.cell(row=x, column=8).value
    abnormRelMME = ws.cell(row=x, column=9).value
    normRel = ws.cell(row=x, column=10).value
    dataPointList.append(dataPoint(time, eNodeBName, cellName, localCellID, abnormRelTot, abnormRelMME, normRel))
print(len(dataPointList))